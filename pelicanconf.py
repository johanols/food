AUTHOR = "Johan"
SITENAME = "food"
SITEURL = "https://food.jmvo.se"
THEME = "theme/nikhil"

PATH = "content"

TIMEZONE = "Europe/Berlin"

DEFAULT_LANG = "en"

SLUGIFY_SOURCE = "basename"

PLUGIN_PATHS = ["plugins"]
PLUGINS = ["microdata"]

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
