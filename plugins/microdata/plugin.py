from pelican import signals
from pelican.contents import Article
from pelican.generators import ArticlesGenerator
import bs4


def article_microdata(generator: ArticlesGenerator):
    new_articles = []
    for article in generator.articles:
        content = bs4.BeautifulSoup(article.content, "html.parser")

        for node in content.find("ol").find_all("li"):
            node.attrs["itemprop"] = "recipeInstructions"
        for node in content.find("ul").find_all("li"):
            node.attrs["itemprop"] = "recipeIngredient"
        for node in content.find_all("img"):
            node.attrs["itemprop"] = "image"
            src = node.attrs["src"]
            if not article.settings["RELATIVE_URLS"] and not src.startswith("http"):
                node.attrs["src"] = f"{article.get_siteurl()}/{src.lstrip('/')}"

        new_articles.append(
            Article(
                str(content),
                metadata=article.metadata,
                settings=article.settings,
                source_path=article.source_path,
                context=article._context
            )
        )
    generator.articles = new_articles


def register():
    signals.article_generator_finalized.connect(article_microdata)
