Title: Tagliatelle con ragù alla bolognese
Date: 2024-06-15 20:50
Category: Italian
Summary: Pure comfort food. Brings back so many childhood memories!

### Ingredients (8 servings)

* 500 g mixed ground beef and pork
* 250 g pancetta
* 1 medium size carrot
* 1 celery stalk
* 1 medium size onion
* 200 ml stock (chicken or vegetable)
* 200 ml white wine
* 1 can (400 g) peeled tomatoes
* 400 ml milk
* extra virgin olive oil

### Instructions

1. Finely chop the carrot, celery, onion and pancetta.
2. Heat olive oil in a large pot and add the vegetable.
3. Sauté until soft then add pancetta and ground meat.
4. Cook meat until brown then add wine and recude by half.
5. Purée the peeled tomatoes.
6. Add tomatoes and stock to pot and stir to combine.
7. Cover the pot and simmer (between 70 and 100 °C) for 2 hours.
8. Remove cover, add milk then stir to combine.
9. Simmer uncovered for 1 hour.
10. Season to taste with salt and petter.
11. Combine sauce with tagliatelle. Don't forget to use pasta water for amazing creaminess!


![Tagliatelle con ragù alla bolognese](images/bolognese.jpg "Tagliatelle con ragù alla bolognese")
