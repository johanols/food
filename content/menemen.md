Title: Menemen
Date: 2024-06-01 09:30
Category: Turkish
Summary: Awesome breakfast dish! Gets you ready for the day!

### Ingredients (2-4 people)

* 1 onion (controversial)
* 2-3 green peppers (or 1 bell pepper)
* 2 medium to big size tomatoes
* 1 tbsp. butter
* 100-200 g turkish brined cheese
* 3-4 eggs
* fresh parsley
* extra virgin olive oil
* salt & pepper flakes (or paprika powder)

### Instructions

1. Dice onion and peppers
2. Grate tomatoes
3. Heat frying pan and add olive oil when hot.
4. Add onion to pan. Fry until starting to brown.
5. Add peppers. Fry until soft.
6. Add butter and wait until it has melted.
7. Add grated tomatoes and reduce liquid.
8. Salt & peppar flakes to taste.
9. Crumble cheese and add. Stir and wait until melted.
10. Make molds and add eggs to molds.
11. Break eggs and mix into the sauce. Wait until coagulated.
12. Top with chopped parsley.
13. Serve with turkish flatbread.

![Menemen](images/menemen.jpg "Menemen")
