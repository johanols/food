Title: Pasta with Shrimps
Date: 2023-01-25 20:30
Category: Fusion
Summary: Rich on flavors from tomatoes and wine topped with delicious shrimps!

### Ingredients (2 people)

* 200 g shrimps
* 200 g [tagliatelle](https://en.wikipedia.org/wiki/Tagliatelle)
* 1 onion
* 1 clove of garlic
* 100 ml white wine
* 200 ml cream (fattier is better)
* 2 tbsp. tomato paste
* rosemary
* extra virgin olive oil
* salt & pepper

### Instructions

1. Finely chop onion. A mixer is recommended.
2. Finely chop rosemary.
3. Glaze over onion, shrimps, rosemary and pressed garlic in olive oil.
4. Remove shrimps. Keep warm.
5. Add tomato paste to pan and fry gently.
6. Add white wine and cream. Stir.
7. Boil pasta one minute less than package instruction.
8. Slowly reduce tomato cream sauce.
9. Salt & pepper to taste.
10. Add pasta and some pasta water to the tomato cream sauce.
11. Keep cooking on low heat and stir until sauce is thick and sticks to the pasta.
12. Add shrimps.
13. Serve on plate with some fresh rosemary and grated italian hard cheese.

![Pasta with Shrimps](images/pasta-with-shrimps.jpg "Pasta with Shrimps")
