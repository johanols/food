Title: Pappa al pomodoro
Date: 2022-11-02 19:56
Category: Italian
Summary: Delicious Tuscan dish made with stale bread.

### Ingredients (2 people)

* 400 g whole peeled tomatoes
* 200 g stale bread (chiabatta)
* 200 ml vegetable stock
* 1 onion
* 2 cloves of garlic
* fresh basil
* extra virgin olive oil
* salt & pepper

### Instructions

1. Chop onion.
2. Fry onion in plenty of olive oil until golden brown.
3. Press garlic and add to pan.
4. Mash the whole peeled tomatoes and add to the pan.
5. Salt & pepper to taste.
6. Add vegetable stock.
7. Add fresh basil to taste.
8. Break bread into small chunks and add to pan.
9. Let bread soak the sauce.
10. Mash bread (hint: use a balloon whisk).
11. Add more olive oil if needed.
12. Serve in a deep plate with some fresh basil.

![Pappa al pomodoro](images/pappa-al-pomodoro.jpg "Pappa al pomodoro")
