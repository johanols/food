Title: Pasta alla carbonara
Date: 2022-11-04 22:00
Category: Italian
Summary: The traditional dish and my favorite. No cream!!!

### Ingredients (2 people)

* 200 g of your favorite pasta (spaghetti is recommended)
* 70 g [guanciale](https://en.wikipedia.org/wiki/Guanciale) (alternatively [pancetta](https://en.wikipedia.org/wiki/Pancetta). No bacon!!!)
* 100 g [pecorino](https://en.wikipedia.org/wiki/Pecorino)
* 3 eggs
* pepper

### Instructions

1. Prepare egg and pecorino mixture.
    - Finely grate the pecorino.
    - Crack the eggs into a small bowl and whisk until slightly frothy.
    - Add pecorino to the eggs and mix well.
    - Add generous amout of pepper.
2. Dice the guanciale.
3. Dry fry the guanciale until crispy.
4. Greatly lower heat of the pan.
5. Boil 2 liters of water with 20 g of salt in a LARGE pot.
6. Cook pasta one minute less than [al dente](https://en.wikipedia.org/wiki/Al_dente).
7. Add eggs and pecorino mixture, pasta, and some pasta water to the pan.
8. Gently mix everything together.
9. Keep stiring gently until mixture thickens. Be patient and don't turn up the heat. You don't want to make scrambled eggs!
10. Serve in a deep plate and top it off with some freshly ground pepper.

![Pasta alla carbonara](images/pasta-alla-carbonara.jpg "Pasta alla carbonara")
