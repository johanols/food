Title: Mushroom Risotto
Date: 2024-06-02 22:50
Category: Italian
Summary: Much creamy mushroom risotto!

### Ingredients (2 people)

* 1 onion
* 400 g mushrooms (porcini, champignon or both)
* 300 g carnaroli rice
* 50 g butter
* 50 g parmigiano reggiano
* 750 ml vegetable stock
* 100 ml white wine
* fresh parsley
* extra virgin olive oil
* salt

### Instructions

1. Fry mushroom in olive oil, salt, and some of the parsley.
2. Fry onion in olive oil. Separate pan.
3. Add rice to onion and toast.
4. Deglace with white wine.
5. Cover rice with vegetable stock. Start rice timer.
6. Continue to add vegetable stock until rice is cooked.
7. Add butter and melt.
8. Add parmigiano reggiano and melt.
9. Add mushrooms.
10. Top with parsley.

![Mushroom Risotto](images/mushroom-risotto.jpg "Mushroom Risotto")
