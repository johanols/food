Title: Hot Chocolate
Date: 2023-10-07 16:00
Category: Italian
Summary: Warms heart and soul.

### Ingredients (2-3 servings)

* 1 tbsp. starch
* 2 tbsp. sugar
* 3 tbsp. cocoa powder
* 100 g dark chocolate (70-85%)
* 500 ml milk
* pinch of salt

### Instructions

1. Chop chocolate into small bits.
2. Mix starch, sugar, cocoa powder in a pot.
3. Add a little bit of the milk.
4. Mix well until there are no lumps.
5. Add the rest of the milk and a pinch of salt.
6. Mix well.
7. Bring to a light simmer.
8. Add chocolate to simmering mixture.
9. Remove pot and let chocolate bits melt.
10. Serve in your favorite hot chocolate cups.

![Hot Chocolate](images/hot-chocolate.jpg "Hot Chocolate")
