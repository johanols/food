Title: Pasta with Tomato Sauce
Date: 2022-11-02 23:30
Category: Italian
Summary: Weekday comfort food. It's the kids' favorite!

### Ingredients (2 people)

* 400 g whole peeled tomatoes
* 200 g of your favorite pasta (spaghetti is recommended)
* 2-3 celery stalks
* 2 carrots
* 1 onion
* 2 cloves of garlic
* fresh basil
* extra virgin olive oil
* salt & pepper

### Instructions

1. Prepare the [soffritto](https://en.wikipedia.org/wiki/Mirepoix#Italian_Soffritto)
    - Finely chop onion, celery and carrots. A mixer is recommended.
    - Add a great amount of olive oil to the mixture.
    - Fry mixture until it gives off a pleasant sweet scent.
2. Press garlic and add to pan.
3. Mash the whole peeled tomatoes, add to the pan and reduce heat.
4. Boil 2 liters of water with 20 g of salt in a LARGE pot.
5. Cook pasta one minute less than [al dente](https://en.wikipedia.org/wiki/Al_dente).
6. Add fresh basil to the tomato sauce.
7. Salt & pepper to taste.
8. Add pasta and some pasta water to the tomato sauce.
9. Keep cooking on low heat and stir until sauce is thick and sticks to the pasta.
10. Serve on plate with some fresh basil and grated italian hard cheese.

![Pasta with Tomato Sauce](images/pasta-with-tomato-sauce.jpg "Pasta with Tomato Sauce")
